# Microservice were being deployed in pm2 version 10-alpine is the closest available image in Alpine.
FROM keymetrics/pm2:10-alpine

# Label
LABEL maintainer="LoginRadius <jitender.agarwal@loginradius.com>"

# Run updates and install deps
RUN apk update && apk add git && apk add python && apk add build-base  

# Create base directory and change owner ID and group ID of /home/node/app directory:
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

# Specify working directory for the rest of the docker file
WORKDIR /home/node/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./ 

# If you are building your code for production
RUN npm install --production --no-cache

# copy all contents from current directory to working directory
COPY . /home/node/app/

# Bundle app source
RUN chown -R node:node /home/node/app

# Container to listen on port 3103
EXPOSE 3103

CMD ["pm2-runtime", "start", "server.js"]
